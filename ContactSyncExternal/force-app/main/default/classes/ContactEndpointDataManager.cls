public with sharing class ContactEndpointDataManager {
    
    public static List<ContactEndpointResponse> updatePUTContacts(Map<Id, Contact> updatedCons, Map<Id, Id> originIdByGeneratedId) {
        List<ContactEndpointResponse> result = new List<ContactEndpointResponse>();
        System.debug('START updatePUTContacts');
        System.debug(updatedCons);
        Integer saveResultIndex = 0;
        try {

            // Try update
            List<Database.SaveResult> srList = Database.update(updatedCons.values(), false);
            for(Database.SaveResult eachSr : srList) {
                ContactEndpointResponse conRes = new ContactEndpointResponse();
                conRes.errorMessage = '';
                conRes.hasError = false;

                // If there's an error, set hasError and errorMessage
                if(!eachSr.isSuccess()) { 
                    conRes.errorMessage += 'Error: ';
                    for(Database.Error err : eachSr.getErrors()) {
                        System.debug('Error: ' + err.getStatusCode() + ': ' + err.getMessage());
                        conRes.errorMessage += '\n' + err.getStatusCode() + ': ' + err.getMessage();
                    }
                    conRes.hasError = true;
                }
                // Build response value
                conRes.generatedId = eachSr.getId();
                conRes.data = updatedCons.get(eachSr.getId());
                if(eachSr.getId() != null) {
                    conRes.originId = originIdByGeneratedId.get(eachSr.getId());
                } else {
                    conRes.originId = originIdByGeneratedId.values()[saveResultIndex];
                    // System.debug('origin Id: ' + originIdByGeneratedId.values()[saveResultIndex]);
                }
                System.debug('eachSr.getId(): ' + eachSr.getId());
                System.debug('conRes: ' + conRes);
                result.add(conRes);
                saveResultIndex++;
            }
        } catch(DmlException dmlEx) {
            System.debug('DML Exception!');
            System.debug(dmlEx.getMessage());
        }
        return result;
    }

    public static List<ContactEndpointResponse> updatePOSTContacts(Map<Id, Contact> insertedCons) {
        List<ContactEndpointResponse> result = new List<ContactEndpointResponse>();
        try {
            // Try update
            List<Database.SaveResult> srList = Database.insert(insertedCons.values(), false);
            // Map created contacts by their id, then
            Map<Id, Contact> consByNewId = new Map<Id, Contact>(insertedCons.values());
            Map<Id, Id> originIdByNewId = new Map<Id, Id>();
            for(Id eachOriginId : insertedCons.keySet()) {
                originIdByNewId.put(insertedCons.get(eachOriginId).Id, eachOriginId);
            }
            
            for(Database.SaveResult eachSr : srList) {
                ContactEndpointResponse conRes = new ContactEndpointResponse();
                conRes.errorMessage = '';
                conRes.hasError = false;
                // If there's an error, set hasError and errorMessage
                if(!eachSr.isSuccess()) {
                    conRes.errorMessage += 'Error: ';
                    for(Database.Error err : eachSr.getErrors()) {
                        System.debug('Error: ' + err.getStatusCode() + ': ' + err.getMessage());
                        conRes.errorMessage += '\n' + err.getStatusCode() + ': ' + err.getMessage();
                    }
                    conRes.hasError = true;
                }
                conRes.generatedId = eachSr.getId();
                conRes.data = consByNewId.get(eachSr.getId());
                conRes.originId = originIdByNewId.get(eachSr.getId());
                result.add(conRes);
            }
        } catch(Exception e) {
            System.debug('ERROR:' + e.getMessage());
        }
        return result;
    }

    public static List<ContactEndpointResponse> updateDELETEContacts(List<Id> conIdsToDelete, Map<Id, Id> originIdByNewId) {
        List<ContactEndpointResponse> result = new List<ContactEndpointResponse>();
        try {
                
            List<Database.DeleteResult> srList = Database.delete(conIdsToDelete, false);
            for(Database.DeleteResult eachSr : srList) {
                ContactEndpointResponse conRes = new ContactEndpointResponse();
                conRes.errorMessage = '';
                conRes.hasError = false;

                if(!eachSr.isSuccess()) {
                    conRes.errorMessage += 'Error: ';
                    for(Database.Error err : eachSr.getErrors()) {
                        System.debug('Error: ' + err.getStatusCode() + ': ' + err.getMessage());
                        conRes.errorMessage += '\n' + err.getStatusCode() + ': ' + err.getMessage();
                    }
                    conRes.hasError = true;
                }
                conRes.generatedId = eachSr.getId();
                conRes.originId = originIdByNewId.get(eachSr.getId());
                result.add(conRes);
            }
            
        } catch (DmlException e) {
            System.debug('ERROR deleting contacts!');
            System.debug(e.getMessage());
        }
        return result;
    }
}
