@RestResource(urlMapping='/ContactService/*')
global with sharing class ContactEndpointService {
    
    @HttpPut
    global static void updateContacts() {

        // Declarations
        Map<String, Object> response = new Map<String, Object>();
        Map<Id, Contact> updatedCons = new Map<Id, Contact>();

        // Get writable fields/external field map
        List<String> writableFields = ContactEndpointUtils.getWritableFields();
        Map<String, String> extFieldMap = ContactEndpointUtils.getExternalFieldMap();

        // Parse passed body
        String jsonStr = RestContext.request.requestBody.toString();
        System.debug('body: ' + jsonStr);
        CalloutRequestWrapper contactRes = (CalloutRequestWrapper)JSON.deserialize(jsonStr, CalloutRequestWrapper.class);

        // Map origin Id by the generated ("external" system) Id
        Map<Id, Id> originIdByGeneratedId = new Map<Id, Id>();
        for(Id eachConId : contactRes.idMap[0].keySet()) {
            // System.debug('Internal: ' + eachConId + ' | External: ' + contactRes.idMap[0].get(eachConId));
            originIdByGeneratedId.put(contactRes.idMap[0].get(eachConId), eachConId);
        }

        // Update Contact with new data, then remove updates from fields without write permissions
        for(Contact eachCon : contactRes.cons) {
            Contact updatedCon = eachCon;
            updatedCon = ContactEndpointUtils.convertLookupsToExternalFields(updatedCon);
            updatedCon = ContactEndpointUtils.removeTargetFields(updatedCon, new List<String>());
            updatedCon.Id = contactRes.idMap[0].get(eachCon.Id);
            updatedCons.put(updatedCon.Id, updatedCon); 
        }
        System.debug('updated: ' + updatedCons);

        // Update Contacts and set response
        List<ContactEndpointResponse> res = new List<ContactEndpointResponse>();
        if(updatedCons.values().size() > 0) {
            res = ContactEndpointDataManager.updatePUTContacts(updatedCons, originIdByGeneratedId);
        }
        
        // Set response
        response.put('res', res);
        System.debug('response: ' + response); 
        RestContext.response.addHeader('Content-Type', 'application/json;charset=UTF-8');
        Restcontext.response.responseBody = Blob.valueOf(JSON.serialize(response));
    }

    @HttpPost
    global static void postContacts() {

        // Declarations
        Map<String, Object> response = new Map<String, Object>();

        // Parse passed body
        String jsonStr = RestContext.request.requestBody.toString();
        System.debug('body: ' + jsonStr);
        CalloutRequestWrapper contactRes = (CalloutRequestWrapper)JSON.deserialize(jsonStr, CalloutRequestWrapper.class);
        Map<Id, Contact> insertedCons = new Map<Id, Contact>(contactRes.cons); // Map by original system id

        // Copy passed contact data, then convert lookups as needed and remove nonwritable fields/Id
        for(Id eachConId : insertedCons.keySet()) {
            Contact updatedCon = insertedCons.get(eachConId);
            updatedCon = ContactEndpointUtils.convertLookupsToExternalFields(updatedCon);
            updatedCon = ContactEndpointUtils.removeTargetFields(updatedCon, new List<String>{'Id'});
            insertedCons.put(eachConId, updatedCon);
        }

        // Update Contacts and set response
        List<ContactEndpointResponse> res = new List<ContactEndpointResponse>();
        if(insertedCons.values().size() > 0) {
            res = ContactEndpointDataManager.updatePOSTContacts(insertedCons);
        }

        // Set response
        response.put('res', res);
        System.debug('response: ' + response);
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
    }

    @HttpDelete
    global static void deleteContacts() {

        // Declarations
        Map<String, Object> response = new Map<String, Object>();
        List<Id> conIdsToDelete = new List<Id>();

        // Parse passed body
        String jsonStr = RestContext.request.requestBody.toString();
        System.debug('body: ' + jsonStr);
        CalloutRequestWrapper contactRes = (CalloutRequestWrapper)JSON.deserialize(jsonStr, CalloutRequestWrapper.class);

        // Map origin Id by new ("external" system) Id and build list of Ids to delete
        Map<Id, Id> originIdByNewId = new Map<Id, Id>();
        for(Contact eachCon : contactRes.cons) {
            Id generatedId = Id.valueOf(contactRes.idMap[0].get(eachCon.Id));
            conIdsToDelete.add(generatedId);
            originIdByNewId.put(generatedId, eachCon.Id);
        }
        System.debug('to delete: ' + conIdsToDelete);

        // Update contacts and get response
        List<ContactEndpointResponse> res = new List<ContactEndpointResponse>();
        if(conIdsToDelete.size() > 0) {
            res = ContactEndpointDataManager.updateDELETEContacts(conIdsToDelete, originIdByNewId);
        }
        // Set response
        response.put('res', res);
        System.debug('response: ' + response);
        Restcontext.response.responseBody = Blob.valueOf(JSON.serialize(response));
    }

    public class CalloutRequestWrapper {
        public List<Contact> cons;
        public List<Map<Id, String>> idMap;
    }
}