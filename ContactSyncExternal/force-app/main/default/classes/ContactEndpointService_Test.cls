@isTest
public class ContactEndpointService_Test
{
    private static String CON_1_FIRST_NAME = 'John';
    private static String CON_1_LAST_NAME = 'Smith';
    private static String SALESFORCE_INSTANCE = 'https://mindful-raccoon-a8f3h7-dev-ed.my.salesforce.com';
    private static Integer NUM_STARTING_CONTACTS = 1;
    
    @TestSetup
    static void makeData(){
        Contact con1 = new Contact(LastName=CON_1_LAST_NAME, FirstName=CON_1_FIRST_NAME);
        insert con1;
    }

    @isTest
    static void doesPutRequestUpdateContact()
    {
        Contact con1 = [SELECT Id, LastName FROM Contact WHERE LastName=:CON_1_LAST_NAME];
        String bodyStr = createBodyStr(con1.Id);
        System.debug('body: ' + bodyStr);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = SALESFORCE_INSTANCE + '/services/apexrest/ContactService';
        req.addHeader('Content-Type', 'application/json');
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(bodyStr);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        ContactEndpointService.updateContacts();
        Test.stopTest();

        con1 = [SELECT Id, LastName FROM Contact WHERE Id=:con1.Id];
        System.assertEquals('NewLastName', con1.LastName);
    }

    @isTest
    static void doesPostRequestCreateContact()
    {
        Contact con1 = [SELECT Id, LastName FROM Contact WHERE LastName=:CON_1_LAST_NAME];
        String bodyStr = createBodyStr(con1.Id);
        System.debug('body: ' + bodyStr);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = SALESFORCE_INSTANCE + '/services/apexrest/ContactService';
        req.addHeader('Content-Type', 'application/json');
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(bodyStr);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        ContactEndpointService.postContacts();
        Test.stopTest();

        List<Contact> cons = [SELECT Id, LastName FROM Contact WHERE Id!=:con1.Id AND LastName='NewLastName'];
        System.assert(cons.size() > 0);
    }

    @isTest
    static void doesDeleteRequestDeleteContact()
    {
        Contact con1 = [SELECT Id, LastName FROM Contact WHERE LastName=:CON_1_LAST_NAME];
        String bodyStr = createBodyStr(con1.Id);
        System.debug('body: ' + bodyStr);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = SALESFORCE_INSTANCE + '/services/apexrest/ContactService';
        req.addHeader('Content-Type', 'application/json');
        req.httpMethod = 'DELETE';
        req.requestBody = Blob.valueOf(bodyStr);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        ContactEndpointService.deleteContacts();
        Test.stopTest();

        List<Contact> cons = [SELECT Id, LastName FROM Contact WHERE Id=:con1.Id];
        System.assert(cons.size() == 0);
    }

    private static String createBodyStr(Id contactId) {
        return '{ "cons": [\n' +
              '{ "attributes": { "type": "Contact", "url": "/services/data/v49.0/sobjects/Contact/0033t000035nw7rAAA"},\n' +
                '"AccountId": "0013t00001aCP6JAAW", "Id": "0033t000035nw7rAAA","LastName": "NewLastName",\n' +
                '"External_System_Id__c": "' + contactId + '" }],\n' + 
                '"idMap": [{"0033t000035nw7rAAA": "'  + contactId + '"}]}\n';
    }
}