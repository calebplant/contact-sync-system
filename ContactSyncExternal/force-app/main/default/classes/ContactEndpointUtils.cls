public with sharing class ContactEndpointUtils {

    private static String DEFAULT_FIELD_MAPPING_RECORD_NAME = 'default';

    /* Returns a list of field API names that the user has write permissions on for Contact object*/
    public static List<String> getWritableFields() {
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();
        List<String> writableFields = new List<String>();
        for(Schema.SObjectField fieldRef : fields.values()) {
            Schema.DescribeFieldResult fieldResult = fieldRef.getDescribe();
            if(fieldResult.isUpdateable()) {
                writableFields.add(fieldResult.getname());
            }
        }
        return writableFields;
    }

    /* Returns a map of field API names for fields that are lookups on the "internal" system
        Example: AccountId, OwnerId => {AccountId=AccountId_x__c, OwmerId=OwnerId_x__c}
        Which fields are mapped (and their name) is set on the Contact_Setting__mdt default record
    */
    public static Map<String, String> getExternalFieldMap() {
        // Declarations
        Map<String, String> result = new Map<String, String>();

        // Get fields to query from Contact Settings custom metadata
        String query = 'SELECT Mapped_Fields__c FROM Contact_Setting__mdt WHERE DeveloperName =:DEFAULT_FIELD_MAPPING_RECORD_NAME LIMIT 1';
        Contact_Setting__mdt conSettings = Database.query(query);
        String queryFields = '';
        for(String eachField : conSettings.Mapped_Fields__c.split(',')) {
            queryFields += eachField + '_Map__c,';
        }
        queryFields += 'Id';

        // Query fields
        query = 'SELECT ' + queryFields + ' FROM Contact_Setting__mdt WHERE DeveloperName =:DEFAULT_FIELD_MAPPING_RECORD_NAME LIMIT 1';
        conSettings = Database.query(query);
        // Map queried fields
        Map<String, Object> fieldValueByName = conSettings.getPopulatedFieldsAsMap();
        for(Object eachField : fieldValueByName.values()) {
            result.put(eachField + '', eachField + '_x__c');
        }
        return result;
    }

    /* Returns a Contact object after removing fields whose name is passed inside targetFields. 
        Also removes fields the user cannot write to.
    */
    public static Contact removeTargetFields(Contact con, List<String> targetFields) {
        System.debug('removeTargetFields...');
        List<String> writableFields = getWritableFields();
        Map<String, Object> conMap = (Map<String, Object>) JSON.deserializeUntyped( JSON.serialize( con ) );
        System.debug(conMap);

        for(String eachField : conMap.keySet()) {
            if(targetFields.contains(eachField) || !writableFields.contains(eachField)) {
                conMap.remove(eachField);
            }
        }
        Contact newCon = (Contact) JSON.deserialize(JSON.serialize(conMap), Contact.class);
        System.debug(newCon);
        return newCon;
    }

    /* Returns Contact with names of lookup fields transformed to their external Id counterparts
        Mapping is set in the custom metadata Contact_Setting__mdt default record
        example: AccountId, OwnerId => {AccountId=AccountId_x__c, OwmerId=OwnerId_x__c}
    */
    public static Contact convertLookupsToExternalFields(Contact con) {
        Map<String, String> extFieldMap = getExternalFieldMap();
        Map<String, Object> conMap = (Map<String, Object>) JSON.deserializeUntyped( JSON.serialize( con ) );
        System.debug('convertLookupsToExternalFields');

        for(String eachField : conMap.keySet()) {
            if(getExternalFieldMap().keySet().contains(eachField)) {
                String extId = (String)conMap.get(eachField);
                conMap.remove(eachField);
                conmap.put(extFieldMap.get(eachField), extId);
            }
        }
        Contact newCon = (Contact) JSON.deserialize(JSON.serialize(conMap), Contact.class);
        System.debug('newCon: ' + newCon);
        return newCon;
    }
}
