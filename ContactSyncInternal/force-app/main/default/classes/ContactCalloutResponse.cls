public with sharing class ContactCalloutResponse {
    public Contact data;
    public Boolean hasError;
    public String errorMessage;
    public Id originId;
    public Id generatedId;

    // public ContactCalloutResponse() {}
}
