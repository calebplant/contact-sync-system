public with sharing class ContactCalloutService implements Queueable, Database.AllowsCallouts {
    
    private Map<Id, Contact> oldConById;
    private Map<Id, Contact> newConById;
    private RequestType reqType;
    
    public ContactCalloutService(Map<Id, Contact> oldConById, Map<Id, Contact> newConById, RequestType reqType) {
        this.oldConById = oldConById;
        this.newConById = newConById;
        this.reqType = reqType;
    }

    public void execute(QueueableContext context) {
        List<Contact> consToUpdate = new List<Contact>();
        System.debug('execute...' + 'type: ' + reqType + ' newConById: ' + newConById);
        
        Map<String, List<Object>> payload = new Map<String, List<Object>>();
        // Map external system ID by internal Id
        Map<Id, String> ExternalSystemIdByInternalId = new Map<Id, String>();
        for(Id eachInternalId : newConById.keySet()) {
            ExternalSystemIdByInternalId.put(eachInternalId, newConById.get(eachInternalId).External_System_Id__c);
        }
        List<Map<Id, String>> idMapPayload = new List<Map<Id, String>>();
        idMapPayload.add(ExternalSystemIdByInternalId);
        // Setup payload
        payload.put('idMap', idMapPayload);
        payload.put('cons', newConById.values());
        // System.debug('payload: ' + payload);
        
        // Send request
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:Contact_Sync_External_API/services/apexrest/ContactService');
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(JSON.serialize(payload));
        req.setMethod(getRequestType());
        Http http = new Http();
        HttpResponse res = http.send(req);
        System.debug('sent');
        
        ContactCalloutResponseWrapper contactRes = (ContactCalloutResponseWrapper)JSON.deserialize(res.getBody(), ContactCalloutResponseWrapper.class);
        System.debug('body: ' + res.getBody());
        System.debug('contactRes: ' + contactRes);
        switch on reqType {
            when POST {
                ContactCalloutService_Handler.handlePOST(contactRes, newConById);
            }
            when PUT {
                ContactCalloutService_Handler.handlePUT(contactRes, newConById, oldConById);
            }
            when DEL {
                ContactCalloutService_Handler.handleDELETE(contactRes, newConById);
            }
            when else {
                System.debug('unknown request type!');
            }
        }
    }

    private String getRequestType() {
        switch on this.reqType {
            when GET {
                return 'GET';
            }
            when POST {
                return 'POST';
            }
            when PUT {
                return 'PUT';
            }
            when DEL {
                return 'DELETE';
            }
            when else {
                System.debug('unknown request type!');
                return 'UNKNOWN';
            }
        }
    }
}