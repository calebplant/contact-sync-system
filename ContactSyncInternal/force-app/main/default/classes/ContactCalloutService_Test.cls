@isTest
public with sharing class ContactCalloutService_Test
{
    private static String POST_CON_FIRST_NAME = 'John';
    private static String POST_CON_LAST_NAME = 'Smith';    

    private static String PUT_CON_FIRST_NAME = 'Abe';
    private static String PUT_CON_LAST_NAME = 'Paper';

    private static String DEL_CON_FIRST_NAME = 'Lance';
    private static String DEL_CON_LAST_NAME = 'Reed';

    private static String NEW_LAST_NAME = 'gravy';
    private static Integer NUM_STARTING_CONTACTS = 1;
    private static Id STATIC_EXTERNAL_ID = '0033t000035vGvGXYZ';

    @TestSetup
    static void makeData()
    {
        ContactTrigger_Helper.bypassTrigger = true;
        Contact putCon = new Contact(LastName=PUT_CON_LAST_NAME, FirstName=PUT_CON_FIRST_NAME, External_System_Id__c=STATIC_EXTERNAL_ID);
        insert putCon;
        ContactTrigger_Helper.bypassTrigger = false;
    }

    @isTest
    static void doesPutFailToUpdateContactOnError()
    {
        Contact putCon = [SELECT Id, LastName, External_System_Id__c FROM Contact WHERE LastName=:PUT_CON_LAST_NAME];
        Map<Id, Contact> oldConById = new Map<Id, Contact>();
        oldConById.put(putCon.Id, putCon);
        Map<Id, Contact> newConById = new Map<Id, Contact>();
        newConById.put(putCon.Id, new Contact(LastName=NEW_LAST_NAME, Id=putCon.Id));

        String errorPutRes = '{"res":[{"originId":"'+ putCon.Id +
        '","hasError":true,"generatedId":"0033t000035vGvGAAU","errorMessage":"I have an error!","data":{"attributes":{"type":"Contact","url":"/services/data/v49.0/sobjects/Contact/0033t000035vGvGAAU"},"CleanStatus":"Pending","Email":"abc@def.com","AccountId_x__c":"0013t00001ZoaA4AAJ",\n' +
        '"LastName":"' + NEW_LAST_NAME + '","Id":"0033t000035vGvGAAU","OwnerId_x__c":"0053t000006kl37AAA"}}]}';

        
        EchoHttpMock mockResponse = new EchoHttpMock(200, 'Complete', errorPutRes, null);
        Test.setMock(HttpCalloutMock.class, mockResponse);

        Test.startTest();
        System.debug('oldConById: ' + oldConById);
        System.debug('newConById: ' + newConById);
        ContactCalloutService calloutJob = new ContactCalloutService(oldConById, newConById, RequestType.PUT);
        Id jobId = System.enqueueJob(calloutJob);
        
        Test.stopTest();
        System.debug('after test');

        putCon = [SELECT Id, LastName FROM Contact WHERE FirstName=:PUT_CON_FIRST_NAME];
        System.assertEquals(PUT_CON_LAST_NAME, putCon.LastName);
    }

    @isTest
    static void doesPutUpdateContactOnSuccess()
    {
        Contact putCon = [SELECT Id, LastName, External_System_Id__c FROM Contact WHERE LastName=:PUT_CON_LAST_NAME];
        Map<Id, Contact> oldConById = new Map<Id, Contact>();
        oldConById.put(putCon.Id, putCon);
        Map<Id, Contact> newConById = new Map<Id, Contact>();
        newConById.put(putCon.Id, new Contact(LastName=NEW_LAST_NAME, Id=putCon.Id));

        String successfulPutRes = '{"res":[{"originId":"'+ putCon.Id +
                                        '","hasError":false,"generatedId":"0033t000035vGvGAAU","errorMessage":"","data":{"attributes":{"type":"Contact","url":"/services/data/v49.0/sobjects/Contact/0033t000035vGvGAAU"},"CleanStatus":"Pending","Email":"abc@def.com","AccountId_x__c":"0013t00001ZoaA4AAJ",\n' +
                                        '"LastName":"' + NEW_LAST_NAME + '","Id":"0033t000035vGvGAAU","OwnerId_x__c":"0053t000006kl37AAA"}}]}';
        
        EchoHttpMock mockResponse = new EchoHttpMock(200, 'Complete', successfulPutRes, null);
        Test.setMock(HttpCalloutMock.class, mockResponse);

        Test.startTest();
        ContactCalloutService calloutJob = new ContactCalloutService(oldConById, newConById, RequestType.PUT);
        Id jobId = System.enqueueJob(calloutJob);
        
        Test.stopTest();
        System.debug('after test');

        putCon = [SELECT Id, LastName FROM Contact WHERE FirstName=:PUT_CON_FIRST_NAME];
        System.assertEquals(NEW_LAST_NAME, putCon.LastName);
    }

    
    @isTest
    static void doesPostUpdateExternalIdOnSuccess()
    {
        ContactTrigger_Helper.bypassTrigger = true;
        Contact postCon = new Contact(LastName=POST_CON_LAST_NAME, FirstName=POST_CON_FIRST_NAME);
        insert postCon;
        ContactTrigger_Helper.bypassTrigger = false;

        Id generatedId = '0033t000035vGvGAAU';
        String successfulPostRes = '{"res":[{"originId":"'+ postCon.Id +
                                        '","hasError":false,"generatedId":"' + generatedId + '","errorMessage":"","data":{"attributes":{"type":"Contact","url":"/services/data/v49.0/sobjects/Contact/0033t000035vGvGAAU"},"CleanStatus":"Pending","Email":"abc@def.com","AccountId_x__c":"0013t00001ZoaA4AAJ",\n' +
                                        '"LastName":"' + POST_CON_LAST_NAME + '","Id":"0033t000035vGvGAAU","OwnerId_x__c":"0053t000006kl37AAA"}}]}';

        EchoHttpMock mockResponse = new EchoHttpMock(200, 'Complete', successfulPostRes, null);
        Test.setMock(HttpCalloutMock.class, mockResponse);

        Test.startTest();
        Map<Id, Contact> newConById = new Map<Id, Contact>();
        newConById.put(postCon.Id, postCon);

        ContactCalloutService calloutJob = new ContactCalloutService(null, newConById, RequestType.POST);
        Id jobId = System.enqueueJob(calloutJob);

        Test.stopTest();

        System.debug('after test');
        postCon = [SELECT Id, LastName, External_System_Id__c FROM Contact WHERE FirstName=:POST_CON_FIRST_NAME];
        System.assertNotEquals(null, postCon.External_System_Id__c);
        System.assertEquals(generatedId, postCon.External_System_Id__c);
    }

    @isTest
    static void doesPostDeleteContactOnFailure()
    {
        ContactTrigger_Helper.bypassTrigger = true;
        Contact postCon = new Contact(LastName=POST_CON_LAST_NAME, FirstName=POST_CON_FIRST_NAME);
        insert postCon;
        ContactTrigger_Helper.bypassTrigger = false;

        Id generatedId = '0033t000035vGvGAAU';
        String errorPostRes = '{"res":[{"originId":"'+ postCon.Id +
                                        '","hasError":true,"generatedId":"' + generatedId + '","errorMessage":"I HAVE AN ERROR","data":{"attributes":{"type":"Contact","url":"/services/data/v49.0/sobjects/Contact/0033t000035vGvGAAU"},"CleanStatus":"Pending","Email":"abc@def.com","AccountId_x__c":"0013t00001ZoaA4AAJ",\n' +
                                        '"LastName":"' + POST_CON_LAST_NAME + '","Id":"0033t000035vGvGAAU","OwnerId_x__c":"0053t000006kl37AAA"}}]}';

        EchoHttpMock mockResponse = new EchoHttpMock(200, 'Complete', errorPostRes, null);
        Test.setMock(HttpCalloutMock.class, mockResponse);

        Test.startTest();
        Map<Id, Contact> newConById = new Map<Id, Contact>();
        newConById.put(postCon.Id, postCon);

        ContactCalloutService calloutJob = new ContactCalloutService(null, newConById, RequestType.POST);
        Id jobId = System.enqueueJob(calloutJob);
        Test.stopTest();

        System.debug('after test');
        Integer numOfCons = [SELECT COUNT() FROM Contact];
        System.assertEquals(NUM_STARTING_CONTACTS, numOfCons);
    }
    
    @isTest
    static void doesDeleteConfirmDeleteOnSuccess() {
        ContactTrigger_Helper.bypassTrigger = true;
        Contact delCon = new Contact(LastName=DEL_CON_LAST_NAME, FirstName=DEL_CON_FIRST_NAME);
        insert delCon;

        Id generatedId = '0033t000035vGvGAAU';
        String successDeleteRes = '{"res":[{"originId":"' + delCon.Id + '","hasError":false,\n' + 
                                    '"generatedId":"' + generatedId + '","errorMessage":"","data":null}]}';

        EchoHttpMock mockResponse = new EchoHttpMock(200, 'Complete', successDeleteRes, null);

        Test.setMock(HttpCalloutMock.class, mockResponse);
        Map<Id, Contact> delConById = new Map<Id, Contact>();
        delConById.put(delCon.Id, delCon);

        Test.startTest();
        delete delCon;
        ContactTrigger_Helper.bypassTrigger = false;

        ContactCalloutService calloutJob = new ContactCalloutService(null, delConById, RequestType.DEL);
        Id jobId = System.enqueueJob(calloutJob);
        Test.stopTest();

        List<Contact> cons = [SELECT Id FROM Contact];
        System.assertEquals(NUM_STARTING_CONTACTS, cons.size());
    }

    
    @isTest
    static void doesDeleteRollbackDeleteOnFailure() {
        ContactTrigger_Helper.bypassTrigger = true;
        Contact delCon = new Contact(LastName=DEL_CON_LAST_NAME, FirstName=DEL_CON_FIRST_NAME);
        insert delCon;
        ContactTrigger_Helper.bypassTrigger = false;

        Id generatedId = '0033t000035vGvGAAU';
        String errorDeleteRes = '{"res":[{"originId":"' + delCon.Id + '","hasError":true,\n' + 
                                    '"generatedId":"' + generatedId + '","errorMessage":"I HAVE AN ERROR","data":null}]}';

        EchoHttpMock mockResponse = new EchoHttpMock(200, 'Complete', errorDeleteRes, null);

        Test.setMock(HttpCalloutMock.class, mockResponse);
        Map<Id, Contact> delConById = new Map<Id, Contact>();
        delConById.put(delCon.Id, delCon);

        Test.startTest();
        ContactCalloutService calloutJob = new ContactCalloutService(null, delConById, RequestType.DEL);
        Id jobId = System.enqueueJob(calloutJob);
        Test.stopTest();

        List<Contact> cons = [SELECT Id FROM Contact];
        delCon = [SELECT Id, Sync_Status__c FROM Contact WHERE LastName=:DEL_CON_LAST_NAME];
        System.assertEquals(NUM_STARTING_CONTACTS + 1, cons.size());
        System.assertEquals('Error', delCon.Sync_Status__c);
    }
    
}