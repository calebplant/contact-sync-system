public with sharing class ContactDeleteHelper implements Queueable {

    private Set<Id> conIdsToUndelete;

    public ContactDeleteHelper(Set<Id> conIdsToUndelete) {
        this.conIdsToUndelete = conIdsToUndelete;
    }

    public void execute(QueueableContext context) {
        System.debug('Execute ContactDeleteHelper');
        List<Contact> consToUndelete = [SELECT Id FROM Contact WHERE Id IN :conIdsToUndelete ALL ROWS];
        undelete consToUndelete;
    }
}