public class ContactTrigger_Helper {
    
    public static Boolean inRecursiveUpdate = false;

    @TestVisible
    static Boolean bypassTrigger=false;

    public static void beforeUpdate(Map<Id, Contact> oldConById, Map<Id, Contact> newConById) {
        System.debug('START beforeUpdate | inRecursiveUpdate: ' + inRecursiveUpdate + ' | bypassTrigger: ' + bypassTrigger);
        if (inRecursiveUpdate || bypassTrigger) return;

        // Roll back changes
        for(Contact eachCon : newConById.values()) {
            eachCon = oldConById.get(eachCon.Id);
        }

        // Callout
        System.debug('Making callout');
        ContactCalloutService calloutJob = new ContactCalloutService(oldConById, newConById, RequestType.PUT);
        Id jobId = System.enqueueJob(calloutJob);
    }

    public static void afterInsert(Map<Id, Contact> newConById) {
        System.debug('START afterInsert | inRecursiveUpdate: ' + inRecursiveUpdate + ' | bypassTrigger: ' + bypassTrigger);
        if (inRecursiveUpdate || bypassTrigger) return;

        // Callout
        System.debug('Making callout');
        ContactCalloutService calloutJob = new ContactCalloutService(null, newConById, RequestType.POST);
        Id jobId = System.enqueueJob(calloutJob);
    }

    public static void afterDelete(Map<Id, Contact> deletedConsById) {
        System.debug('START afterDelete | inRecursiveUpdate: ' + inRecursiveUpdate + ' | bypassTrigger: ' + bypassTrigger);
        if (inRecursiveUpdate || bypassTrigger) return;

        // Rollback deletes
        ContactDeleteHelper deleteJob = new ContactDeleteHelper(deletedConsById.keySet());
        System.enqueueJob(deleteJob);
        // Callout
        System.debug('Making callout');
        ContactCalloutService calloutJob = new ContactCalloutService(null, deletedConsById, RequestType.DEL);
        system.enqueueJob(calloutJob);
        System.debug('END afterDelete');
    }
}