@isTest
public with sharing class ContactTrigger_Test {

    private static String PUT_CON_FIRST_NAME = 'Abe';
    private static String PUT_CON_LAST_NAME = 'Paper';

    private static String POST_CON_FIRST_NAME = 'John';
    private static String POST_CON_LAST_NAME = 'Smith';

    private static String FAILURE_POST_RESPONSE = '{"message":"Error on POST!","hasError":true}';
    private static String FAILURE_PUT_RESPONSE = '{"message":"Error on PUT!","hasError":true}';
    private static String SUCCESSFUL_POST_RESPONSE = '{"message":"Error on PUT!","hasError":true}';

    private static Integer NUM_STARTING_CONTACTS = 1;

    @TestSetup
    static void makeData()
    {
        ContactTrigger_Helper.bypassTrigger = true;
        Contact putCon = new Contact(LastName=PUT_CON_LAST_NAME, FirstName=PUT_CON_FIRST_NAME);
        insert putCon;
        ContactTrigger_Helper.bypassTrigger = false;
    }

    @isTest
    static void doesContactInsertTriggerPostRequest() {
        Contact postCon = new Contact(LastName=POST_CON_LAST_NAME, FirstName=POST_CON_FIRST_NAME);
        EchoHttpMock mockResponse = new EchoHttpMock(200, 'Complete', FAILURE_POST_RESPONSE, null);
        Test.setMock(HttpCalloutMock.class, mockResponse);

        Test.startTest();
        insert postCon;
        
        List<AsyncApexJob> jobInfo = [SELECT Status, NumberOfErrors FROM AsyncApexJob];
        System.assertEquals(1, jobInfo.size());
    }

    @isTest
    static void doesContactUpdateTriggerPutRequest() {
        Contact putCon = [SELECT Id, LastName FROM Contact WHERE LastName=:PUT_CON_LAST_NAME];
        putCon.LastName = 'bananas';
        EchoHttpMock mockResponse = new EchoHttpMock(200, 'Complete', FAILURE_PUT_RESPONSE, null);
        Test.setMock(HttpCalloutMock.class, mockResponse);

        Test.startTest();
        update putCon;
        
        List<AsyncApexJob> jobInfo = [SELECT Status, NumberOfErrors FROM AsyncApexJob];
        System.assertEquals(1, jobInfo.size());
    }

    @isTest
    static void doesDeleteTriggerDeleteRequest() {
        System.debug('Start doesDeleteTriggerRollbackDeleteOnFailure');
        Contact putCon = [SELECT Id, LastName FROM Contact WHERE LastName=:PUT_CON_LAST_NAME];
        EchoHttpMock mockResponse = new EchoHttpMock(200, 'Complete', FAILURE_PUT_RESPONSE, null);
        Test.setMock(HttpCalloutMock.class, mockResponse);

        Test.startTest();
        delete putCon;

        List<AsyncApexJob> jobInfo = [SELECT Status, NumberOfErrors FROM AsyncApexJob];
        System.assertEquals(2, jobInfo.size());
    }
}