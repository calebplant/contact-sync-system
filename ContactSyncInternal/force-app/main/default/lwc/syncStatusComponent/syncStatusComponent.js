import { LightningElement, api, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import SYNC_STATUS_FIELD from '@salesforce/schema/Contact.Sync_Status__c';
import SYNC_ERROR_FIELD from '@salesforce/schema/Contact.Sync_Error__c';

const FIELDS = [SYNC_STATUS_FIELD, SYNC_ERROR_FIELD];

export default class SyncStatusComponent extends LightningElement {
    @api recordId;

    syncStatus;
    syncError;

    @wire(getRecord, {recordId: '$recordId', fields: FIELDS})
    getContact(response) {
        if(response.data) {
            console.log('Response: ');
            console.log(response.data);
            
            this.syncStatus = response.data.fields.Sync_Status__c.value;
            this.syncError = response.data.fields.Sync_Error__c.value;
        }
        if(response.error) {
            console.log('Error:');
            console.log(response.error);
        }
    }

    get hasSyncError() {
        console.log('evaluated:');
        console.log(this.syncStatus === 'Error');
        
        return this.syncStatus === 'Error' ? true : false;
    }
}