trigger ContactTrigger on Contact (after insert, before update, after delete) {
    if(Trigger.isBefore && Trigger.isUpdate) {
        ContactTrigger_Helper.beforeUpdate(Trigger.oldMap, Trigger.newMap);
    } else if (Trigger.isAfter && Trigger.isInsert) {
        ContactTrigger_Helper.afterInsert(Trigger.newMap);
    } else if (Trigger.isAfter && Trigger.isDelete) {
        ContactTrigger_Helper.afterDelete(Trigger.oldMap);
    }
}