# Contact Sync Task

## Overview

![Overview Diagram](media/overview.png)

The process follows the following steps

1. Contact Trigger invokes the Helper based on context
1. The Helper rolls back changes and enqueues the Callout Service
1. The Callout Service passes the updated Contacts to the external REST API
1. The external system attempts to update the passed Contacts. If an update fails on a record, it marks the record with an error state and message.
1. The external system sends a response back to the Callout Service with a list of records and their error state.
1. The Callout Service invokes the Callout Handler, which updates records on the internal database based on the response.